/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1javafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 *
 * @author Facry
 */
public class TP1JAVAFX extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            //primaryStage est la fenêtre principale 
            primaryStage.setTitle("Application changement de caractère");

            //Création des différentes polices, background et bordures 
            Font police1 = Font.font("Arial", FontWeight.BOLD, 14);
            Font police2 = Font.font("Arial", FontWeight.BOLD, 12);
            Background fond = new Background(new BackgroundFill(Color.CORNFLOWERBLUE, null, new Insets(5)));
            Background fondBouton = new Background(new BackgroundFill(Color.GREEN, null, new Insets(0)));
            Border bordLabel = new Border(new BorderStroke(Color.STEELBLUE, BorderStrokeStyle.SOLID, new CornerRadii(3), BorderStroke.MEDIUM));

            //Création d'un panneau vertical pour y ajouter tous nos composants
            VBox root = new VBox();
            root.setAlignment(Pos.CENTER);
            root.setBackground(fond);

            //Création d'un décor qui contiendra le panneau principal root (créé précédemment)
            Scene scene = new Scene(root, 350, 200);

            //Création d'un panneau sous forme de tableau (2x2)
            GridPane grid = new GridPane();
            grid.setGridLinesVisible(true);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));

            //Création des différents élèments à ajouter au panneau grid
            Label lSaisirText = new Label(" Texte à modifier : ");
            lSaisirText.setFont(police2);

            TextField txtSaisie = new TextField();
            Button bMinuscule = new Button("Minuscules");
            Button bMajuscule = new Button("Majuscules");
            bMajuscule.setBackground(fondBouton);
            bMajuscule.setTextFill(Color.WHITE);
            bMajuscule.setFont(police2);
            bMinuscule.setBackground(fondBouton);
            bMinuscule.setTextFill(Color.WHITE);
            bMinuscule.setFont(police2);
            //Création dde deux panneaux pour ajouter et centrer chacun des bouton
            HBox hbBMajuscule = new HBox(10);
            hbBMajuscule.setAlignment(Pos.CENTER);
            HBox hbBMinuscule = new HBox(10);
            hbBMinuscule.setAlignment(Pos.CENTER);

            // ajout des boutons à chaque panneau
            hbBMajuscule.getChildren().add(bMajuscule);
            hbBMinuscule.getChildren().add(bMinuscule);

            //ajout des différents élements au panneau grid
            grid.add(lSaisirText, 0, 0);
            grid.add(txtSaisie, 1, 0);
            grid.add(hbBMajuscule, 0, 1);
            grid.add(hbBMinuscule, 1, 1);

            //ajout du panneau grid au panneau principal
            root.getChildren().add(grid);

            //création du dernier composant pour afficher le texte modifié
            Label lTexteModifie = new Label(" Vide ");
            lTexteModifie.setMinSize(100, 30);
            lTexteModifie.setBorder(bordLabel);
            lTexteModifie.setTextFill(Color.FIREBRICK);
            lTexteModifie.setFont(police1);
            root.getChildren().add(lTexteModifie);
			
			//Gestion des événements
            bMajuscule.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    String saisie = txtSaisie.getText();
                    String affichageMaj = saisie.toUpperCase();
                    lTexteModifie.setText(affichageMaj);
                }
            });
            
             bMinuscule.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    String saisie = txtSaisie.getText();
                    String affichageMin = saisie.toLowerCase();
                    lTexteModifie.setText(affichageMin);
                }
            });
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}