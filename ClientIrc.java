package chat3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;





/**
 *
 * @author facry
 */
public class Client {

    BufferedWriter writer;
    BufferedReader reader;
    Socket socket;
    String channel,nick;
    //ArrayList<String> users = new ArrayList();
    
    //------------------------------ Constructeur ------------------------------\\
    /**
     * Constructeur à 4 arguments de la classe Client
     * @param serveur
     * @param port
     * @param channel
     * @param nick
     * @throws IOException 
     */
    private Client(String serveur, int port, String channel, String nick) throws IOException{
        this.socket = new Socket(serveur, port);
        this.channel = channel; 
        this.nick = nick;
        this.socket.setSoTimeout(10000);
        this.writer = new BufferedWriter (new OutputStreamWriter(socket.getOutputStream()));
        this.reader = new BufferedReader (new InputStreamReader(socket.getInputStream()));
        
            
    }
    
    //------------------------------  Creation de la connexion au serveur ------------------------------\\
    
    /**
     * cette classe va permettre la création d'une connexion au serveur 
     * @param nick
     * @param login
     * @param channel
     * @param serveur
     * @param port
     * @return
     * @throws IOException 
     */
    public static Client CC(String nick, String login, String channel, String serveur, int port) throws IOException {
        
        Client tchat = new Client (serveur, port,  channel, nick);
        tchat.writer.write("NICK " + nick + "\r\n");
        tchat.writer.write("USER " + login + " 8 * : Java IRC Hacks Bot\r\n");
        tchat.writer.flush( );
        
        // Récupération des infos pour voir si il est connecté au serveur
        
        String line = null;
        while ((line = tchat.reader.readLine( )) != null) {
            if (line.indexOf("004") >= 0) {
                // We are now logged in.
                break;
            }
            else if (line.indexOf("433") >= 0) {
                System.out.println("Nickname is already in use.");
                break;
            }
        }
        
    // ------------------------------ Rejoindre le channel ------------------------------\\
        
        tchat.writer.write("JOIN " + channel + "\r\n");
        tchat.writer.flush( );
        return tchat;
        
    }
    
    
    //------------------------------  Envoi de messages ------------------------------\\
    
    /**
     * Cette classe va permettre l'envoi de messages
     * @param message
     * @throws IOException 
     */
    public void  EM(String message) throws IOException{
      
      getWriter().write("PRIVMSG " + this.channel + " : "+ message + "\r\n");
      getWriter().flush();
      System.out.println(this.nick + ":" + message);
    }
        
  //------------------------------ Affichage des Messages ------------------------------\\
 /**
  * Permet de lire les messages 
  * @return
  * @throws IOException 
  */
    public String read() throws IOException{
        String line;
    
     while ((line = this.getReader().readLine( )) != null) {
            if (line.toLowerCase( ).startsWith("PING ")) {
                // We must respond to PINGs to avoid being disconnected.
                this.getWriter().write("PONG " + line.substring(5) + "\r\n");
                this.getWriter().write("PRIVMSG " + channel + " :I got pinged!\r\n");
                this.getWriter().flush();
            }
            else {
                // Affiche les messages
                return line;
            }
        }   
     return line;
    }
     
    //------------------------------ Sauvegarder un Fichier ------------------------------\\ 
    /**
     * Cette classe va permettre de sauvegarder les conversations sur le chat ainsi que les logs
     * @param content
     * @param file 
     */
     public void SaveFile(String content, File file){
        try {
            FileWriter fileWriter = null;
             
            fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(CHAT3.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }
     
    
     //------------------------------ Se déconnecter  ------------------------------\\ 
    
     /**
      * La classe Quit va permettre la déconnexion propre de l'application
      * @throws IOException 
      */
       public void quit() throws IOException {
           writer.write("/quit");
           writer.flush();
           this.socket.close();
       }
    

   
    
     //------------------------------ Traitement des RawLog ------------------------------\\  
   /**
    * Cette classe permet de traiter les logs, pour avoir ainsi des messages propres avec le pseudo suivi du messages
    * @param msg
    * @return 
    */
     public  String TraitRL(String msg){
         
         
         if(msg.split(":")[1].length()!=3){
             if(msg.split(":")[1].contains("PRIVMSG")){
                 String msg1 = msg.split(":")[1];
                 String msg2 = msg.split(":")[2];
                 String msg3 = msg1.split("!")[0];
                 msg = msg3 + " : " + msg2;
                 return msg;
             }
             else if (msg.split(":")[1].contains("moulinsart.traydent.info")){
                 String msg1 = msg.split(":")[1];
                 String msg2 = msg.split(":")[2];
                 
                 msg =  msg2;
                 return msg;
             }
                 
         }         
         
         //return " ";
         return msg;
     }
          
 
    
    /**
     * 
     * @return 
     */
    public BufferedWriter getWriter(){
        return this.writer;
    }
    
    /**
     * 
     * @return 
     */
    public BufferedReader getReader(){
        return this.reader;
    }


	
    }

   
    
    
  