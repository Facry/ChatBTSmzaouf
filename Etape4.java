/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bts.si56.serveur.etape4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;


/**
 *
 * @author Facry 
 */

public class ServeurMATH {
    
    
   

    public static void main(String[] args) throws IOException {
        //creation de l'adresse ip de la machine hote accueillant le serveur
        InetAddress hote = InetAddress.getByName("10.0.23.13");
        //InetAddress localhost = InetAddress.getLoopbackAddress();
        int port = Integer.parseInt(args[0]);
        System.out.println("hote : " + hote.toString());
        //System.out.println("loop back retourne : " + localhost.toString());

        //creer une socket de communication sur ce serveur sur un port donné
        //connection etablie 
        Socket sClient = new Socket(hote, port);
        System.out.println("socket connecté ? : " + sClient.isConnected());
        // le client se connecte au serveur 
        InputStream inStream = sClient.getInputStream();
        OutputStream outStream = sClient.getOutputStream();
        //creation d'un dataStream pour acceder à des fonctionnalites de lecture et d'ecriture supplementaires
        DataInputStream dataIn = new DataInputStream(inStream);
        DataOutputStream dataOut = new DataOutputStream(outStream);

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("entrer la fonction : entier1 entier2 :");
            String chainSaisi = sc.nextLine();
            //TODO verification que la ligne correspond bien à ce qu'on attend
            dataOut.writeUTF(chainSaisi);
            String retourServeur = dataIn.readUTF();
            System.out.println(retourServeur);
            if (retourServeur.equals("FIN")) {
                sClient.close();
                break;
            }

        }

    }
}