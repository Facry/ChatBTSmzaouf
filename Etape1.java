/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.*/
package bts.si56.tpX.Etape1;

/**
 *
 * @author facry
 */
public class Etape1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       System.out.println(somme(10, 5));
       System.out.println(difference(10, 5));
       System.out.println(produit(10, 5)) ; 
       System.out.println(quotient(10, 5)); 
    }
               
      public static int somme(int a, int b) {
        return a + b;
    }

    public static int difference(int a, int b) {
        return b - a; 
    }

    public static int produit(int a, int b) {
        return a * b; 
    }

    public static int quotient(int a, int b) {
        return b / a ;
    }
}
